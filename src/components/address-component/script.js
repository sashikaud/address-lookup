const apiUrl = 'http://address-service.com/suggestAddress';

const addressSuggestions = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: `${apiUrl}?userAddress=%QUERY`,
        wildcard: '%QUERY'
    }
});


$('#userAddress').typeahead({
    minLength: 3,
    hint: false,
}, {
    name: 'address-suggestions',
    source: addressSuggestions
}).on('typeahead:autocomplete', function (event, suggestion) {
    $('#userAddress').val(suggestion);
}).on('typeahead:selected', function (event, suggestion) {
    $('#userAddress').val(suggestion);
});

